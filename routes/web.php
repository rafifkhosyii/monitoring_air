<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tangki', 'InventoriController@indexTangki')->name('inventori.indexTangki');
Route::get('/device', 'InventoriController@indexDevice')->name('inventori.indexDevice');

Route::post('/tambah-device', 'InventoriController@tambahDevice')->name('tambahDevice');
Route::post('/tambah-tangki', 'InventoriController@tambahTangki')->name('tambahTangki');
Route::post('/tambah-monitor', 'InventoriController@tambahMonitor')->name('tambahMonitor');
Route::post('/tambah-stock-device', 'InventoriController@tambahStockDevice')->name('tambahStockDevice');
Route::post('/hapus-device', 'InventoriController@hapusDevice')->name('hapusDevice');
Route::post('/hapus-tangki', 'InventoriController@hapusTangki')->name('hapusTangki');
Route::post('/cabut-device', 'InventoriController@dismantleDevice')->name('dismantleDevice');