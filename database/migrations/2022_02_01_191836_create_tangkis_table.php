<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTangkisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tangkis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_tangki')->nullable();
            $table->string('nama_tangki')->nullable();
            $table->string('jenis_tangki')->nullable();
            $table->integer('max_kapasitas')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tangkis');
    }
}
