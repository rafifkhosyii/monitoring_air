<?php

use Illuminate\Database\Seeder;
use App\LogMonitor;
use App\Monitor;
use App\Device;
use App\Tangki;
use Faker\Factory;
class MonitorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        for ($i=0; $i < 4 ; $i++) { 
           $device = Device::create([
            'kode_device' => uniqid('#DV_'),
            'nama_device' => 'Sensor Monitoring '.$i,
            'qty' => 10,
            'status' => 1,
        ]);
       }

       $jenis_tangki = ['Tandon Air Fiber Glass','Tandon Air Polyethylene','Tandon Air Stainless Steel'];

       for ($i=0; $i < 4 ; $i++) { 
           $tangki = Tangki::create([
            'kode_tangki' => uniqid('#TK_'),
            'nama_tangki' => 'Tangki Air '.$i,
            'jenis_tangki' => $faker->randomElements($jenis_tangki)[0],
            'max_kapasitas' => rand(1000,5000),
            'status' => 1,
        ]);
       }

       $monitor = Monitor::create([
            'id_tangki'  => 1,
            'id_device' => 2,
            'cur_cap' => rand(500,1200),
            'cur_temp' => rand(20,50),
            'cur_height' => rand(0,10),
            'status' => 1,
        ]);

       $update = Tangki::where('id',1)->update([
        'status'=> 2,
       ]);

       $log = LogMonitor::create([
            'deskripsi'  => 'Data Tangki telah diperbaharui',
        ]);

    }
}
