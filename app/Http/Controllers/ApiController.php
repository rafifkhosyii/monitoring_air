<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LogMonitor;
use Carbon\Carbon;

class ApiController extends Controller
{
    public function apiLogMonitor()
    {
        $daftar_log = [];
        $get_log = LogMonitor::orderBy('id','DESC')->get();
        foreach ($get_log as $key => $br) {
            $daftar_log[$key]= [
                'id' => $br->id,
                'id_monitor' => $br->id_monitor,
                'deskripsi' => $br->deskripsi,
                'created_at' => $br->created_at->format('d-M-y H:i'),
            ];
        }
        return response()->json($daftar_log, 200);
    }
}
