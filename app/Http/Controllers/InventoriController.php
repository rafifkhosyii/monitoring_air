<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Tangki;
use App\LogMonitor;
use App\Monitor;
use Auth;
use Alert;
use Carbon\Carbon;

class InventoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexTangki()
    {
        $get_tangki = Tangki::orderBy('id','DESC')->get();
        $get_tangki_aktif = Tangki::where('status',2)->get();
        return view('inventori.index-tangki',compact('get_tangki','get_tangki_aktif'));
    }
    public function indexDevice()
    {
        $get_device = Device::orderBy('id','DESC')->get();
        return view('inventori.index-device',compact('get_device'));
    }

    public function tambahDevice(Request $request)
    {
        $kode_device = uniqid('#DV_');
        $create_device = Device::create([
            'kode_device' => $kode_device,
            'nama_device' => $request->nama_device,
            'qty' => $request->qty,
            'status' => 1,
        ]);
        return redirect()->back()->with('toast_success','Device baru berhasil ditambahkan');
    }
    public function tambahStockDevice(Request $request)
    {
        $get_device = Device::where('id',$request->id_device)->first();
        $update_device = $get_device->update([
            'status' => 1,
            'qty' => $get_device->qty+$request->qty,
        ]);
        return redirect()->back()->with('toast_success','Stok Device behasil diperbaharui');
    }
    public function hapusDevice(Request $request)
    {
        $get_device = Device::where('id',$request->id_device)->first();
        $delete_monitor = Monitor::where('id_device',$request->id_device)->delete();
        $delete_device = Device::where('id',$request->id_device)->delete();

        $log_monitor = LogMonitor::create([
            'deskripsi' => 'Data device '.$get_device->nama_device.' telah dihapus',
        ]);
        return redirect()->back()->with('toast_success','Device berhasil dihapus');
    }
    public function hapusTangki(Request $request)
    {
        $get_tangki = Tangki::where('id',$request->id_tangki)->first();
        $delete_monitor = Monitor::where('id_tangki',$request->id_tangki)->delete();
        $delete_tangki = Tangki::where('id',$request->id_tangki)->delete();

        $log_monitor = LogMonitor::create([
            'deskripsi' => 'Data Tangki '.$get_tangki->nama_tangki.' telah dihapus',
        ]);
        return redirect()->back()->with('toast_success','Tangki berhasil dihapus');
    }
    public function dismantleDevice(Request $request)
    {
        $get_monitor = Monitor::where('id',$request->id_monitor)->first();
        $get_tangki = Tangki::where('id',$get_monitor->id_tangki)->first();
        $get_device = Device::where('id',$get_monitor->id_device)->first();

        $update_tangki = $get_tangki->update([
            'status' => 1,
        ]);
        $update_device = $get_device->update([
            'status' => 1,
            'qty' => $get_device->qty+1,
        ]);
        $hapus_monitor = $get_monitor->delete();

        $log_monitor = LogMonitor::create([
            'deskripsi' => 'Device pada tangki '.$get_tangki->nama_tangki.' telah didismantle',
        ]);
        return redirect()->back()->with('toast_success','Device pada tangki berhasil didismantle');
    }
    public function tambahTangki(Request $request)
    {
        $kode_tangki = uniqid('#TK_');
        $create_tangki = Tangki::create([
            'kode_tangki' => $kode_tangki,
            'nama_tangki' => $request->nama_tangki,
            'max_kapasitas' => $request->max_kapasitas,
            'jenis_tangki' => $request->jenis_tangki,
            'status' => 1,
        ]);
        return redirect()->back()->with('toast_success','Tangki baru berhasil ditambahkan');
    }
    public function tambahMonitor(Request $request)
    {   
        $get_tangki = Tangki::where('id',$request->id_tangki)->first();
        $get_device = Device::where('id',$request->id_device)->first();
        $create_monitor = Monitor::create([
            'id_tangki' => $request->id_tangki,
            'id_device' => $request->id_device,
            'cur_temp' => rand(20,50),
            'cur_cap' => rand(0,$get_tangki->max_kapasitas),
            'cur_height' => rand(0,10),
            'status' => 1,
        ]);

        $update_tangki = $get_tangki->update([
            'status' => 2,
        ]);

       if ($get_device->qty - 1 < 1) {
             $update_device = Device::where('id',$request->id_device)->update([
                'qty' => 0,
                'status' => 0,
             ]);
        }else{
            $update_device = Device::where('id',$request->id_device)->update([
                'qty' => $get_device->qty-1,
             ]);
        }

        $log_monitor = LogMonitor::create([
            'deskripsi' => 'Penambahan monitor untuk tangki air '.$get_tangki->nama_tangki.'',
        ]);
        return redirect()->back()->with('toast_success','Monitor tangki air baru berhasil ditambahkan');
    }
}
