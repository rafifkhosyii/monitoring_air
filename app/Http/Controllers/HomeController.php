<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Monitor;
use App\Tangki;
use App\Device;
use App\LogMonitor;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $get_monitor = Tangki::where('status',2)->orderBy('id','DESC')->get();
        $get_device = Device::where('status',1)->orderBy('id','DESC')->get();
        $get_log = LogMonitor::limit(6)->orderBy('id','DESC')->get();
        return view('monitor.overview', compact('get_monitor','get_device','get_log'));
    }
}
