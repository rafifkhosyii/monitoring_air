<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monitor extends Model
{
    protected $fillable=[
        'id_tangki',
        'id_device',
        'cur_cap',
        'cur_temp',
        'cur_height',
        'status',
    ];
}
