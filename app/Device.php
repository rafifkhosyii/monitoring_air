<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable=[
        'kode_device',
        'nama_device',
        'qty',
        'status',
    ];
}
