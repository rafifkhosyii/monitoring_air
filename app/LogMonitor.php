<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogMonitor extends Model
{
    protected $fillable=[
        'id_monitor',
        'deskripsi',
    ];
}
