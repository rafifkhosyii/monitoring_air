<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tangki extends Model
{
    protected $fillable=[
        'kode_tangki',
        'nama_tangki',
        'jenis_tangki',
        'max_kapasitas',
        'status',
    ];
}
