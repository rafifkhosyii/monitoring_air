@extends('layouts.app')

@section('navbar')
@include('layouts.navbar')
@endsection

@section('sidebar')

@endsection

@section('content')

@php
$today = Carbon\Carbon::today();
@endphp
 
<div class="container-fluid pt-5 pb-7 bg-white">
    <div class="row justify-content-center">
        <div class="col-lg-11 col-12">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-12 mb-4">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-12 mb-4">
                                    <div class="row align-items-center">
                                        <div class="col-lg-12 col-12 mb-lg-0 mb-2">
                                            <span class="display-3 font-weight-bolder text-gray-dark">Tangki Air</span>
                                            <div class="mt--1" style="height: 10px; background-color: blue; width: 50px;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="card shadow-none badge-info" style=" background-size:45%; background-repeat:no-repeat; background-position: -30px 50%;background-image: url({{asset('argon')}}/img/icons/tank.png">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-7 col-7 offset-5 offset-md-5 offset-lg-6">
                                                    <div class="row justify-content-between">
                                                        <div class="col-lg-12 col-12 pb-0 pb-lg-4">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-12">
                                                                    <h4 class="mb--1">Total Tangki</h4>
                                                                    <span class="display-2 font-weight-bolder mb-0">{{$get_tangki->count()}}</span>
                                                                </div>
                                                                <div class="col-lg-6 col-12 text-right">
                                                                    <button type="button" class="btn btn-info shadow-none btn-icon-only" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-plus"></i></button>
                                                                    <div class="dropdown-menu">
                                                                        <button class="dropdown-item" data-target=".modal-tambah-tangki" data-toggle="modal"><i class="fas fa-plus"></i>Tambah Tangki Baru</button>
                                                                        <button class="dropdown-item" data-target=".modal-tambah-monitor" data-toggle="modal"><i class="fas fa-prescription-bottle-alt"></i>Tambah Tangki Aktif</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-12 mt-3">
                                    <div class="nav-wrapper">
                                        <ul class="nav nav-tabs" id="tabs-icons-text" role="tablist">
                                            <li class="nav-item mb-0">
                                                <a class="nav-link active text-sm" id="tabs-icons-mts-1-tab" data-toggle="tab" href="#tabs-icons-mts-1" role="tab" aria-controls="tabs-icons-mts-1" aria-selected="true"><i class="fas fa-list mr-2"></i>Semua</a>
                                            </li>
                                            <li class="nav-item mb-0">
                                                <a class="nav-link text-sm" id="tabs-icons-mts-2-tab" data-toggle="tab" href="#tabs-icons-mts-2" role="tab" aria-controls="tabs-icons-mts-2" aria-selected="false"><i class="fas fa-clipboard-check mr-2"></i>Aktif (<b>{{$get_tangki_aktif->count()}}</b>)</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-12 mb-4">
                                    <div class="row align-items-center">
                                        <div class="col-lg-12 col-12">
                                            <div class="tab-content" id="myTabContent">
                                                <div class="tab-pane fade show active" id="tabs-icons-mts-1" role="tabpanel" aria-labelledby="tabs-icons-mts-1-tab">
                                                    <div class="table-responsive pt-3 pb-3">
                                                        <table class=" table table-flush datatable-basic">
                                                            <thead class="thead-light">
                                                                <tr class="text-center">
                                                                    <th>#</th>
                                                                    <th>Tangki</th>
                                                                    <th>Kapasitas</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @php
                                                                $index = 1;
                                                                @endphp

                                                                @foreach($get_tangki as $gt)
                                                                <tr>
                                                                    <td class="text-muted text-center">{{$index++}}</td>
                                                                    <td>
                                                                        <div class="media">
                                                                            <img class="avatar avatar-sm mb-0 bg-transparent" src="{{asset('argon')}}/img/icons/storage-tank.png">
                                                                            <div class="media-body ml-3">
                                                                                <div class="row align-items-center">
                                                                                    <div class="col-lg-12 col-12">
                                                                                        <a href="#!"><h5 class="mb-0">{{$gt->nama_tangki}}</h5></a>
                                                                                        <span class="text-xs text-muted">Total Kapasitas : <b>{{$gt->max_kapasitas}} L</b></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        @if($gt->status == 1)
                                                                        <span class="text-muted text-sm">-</span>
                                                                        @else
                                                                        @php
                                                                        $get_tandon_aktif_1 = App\Monitor::where('id_tangki',$gt->id)->first();
                                                                        @endphp
                                                                        <div class="row">
                                                                            <div class="col-lg-12 col-12 mb-3">
                                                                                <div class="progress-wrapper pt-0">
                                                                                    <div class="progress-info">
                                                                                        <div class="progress-percentage">
                                                                                            <span class="text-xs font-weight-bold text-default">Terpakai : {{round(($get_tandon_aktif_1->cur_cap/$gt->max_kapasitas)*100,2)}}% <sup><i class="fas fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="{{$get_tandon_aktif_1->cur_cap}} L"></i></sup></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar bg-success" role="progressbar" aria-valuenow="{{($get_tandon_aktif_1->cur_cap/$gt->max_kapasitas)*100}}" aria-valuemin="0" aria-valuemax="100" style="width: {{round(($get_tandon_aktif_1->cur_cap/$gt->max_kapasitas)*100,2)}}%;"></div>
                                                                                    </div>
                                                                                    <span class="text-xs text-muted">Tersisa : {{$gt->max_kapasitas-$get_tandon_aktif_1->cur_cap}} L</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12 col-12">
                                                                                <div class="row">
                                                                                    <div class="col-auto">
                                                                                        <div class="media align-items-center">
                                                                                            <div class="icon icon-xs icon-shape badge-primary shadow-none">
                                                                                                <i class="fas fa-thermometer-empty"></i>
                                                                                            </div>
                                                                                            <div class="media-body ml-2">
                                                                                                <span class="text-xs font-weight-bold text-default">Suhu Air</span>
                                                                                                <h4 class="mb-0 mt--1 font-weight-bolder">{{$get_tandon_aktif_1->cur_temp}} °</h4>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-auto">
                                                                                        <div class="media align-items-center">
                                                                                            <div class="icon icon-xs icon-shape badge-primary shadow-none">
                                                                                                <i class="fas fa-water"></i>
                                                                                            </div>
                                                                                            <div class="media-body ml-2">
                                                                                                <span class="text-xs font-weight-bold text-default">Tinggi Air</span>
                                                                                                <h4 class="mb-0 mt--1 font-weight-bolder">{{$get_tandon_aktif_1->cur_height}} M</h4>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @endif 
                                                                    </td>
                                                                    <td class="text-center">
                                                                        @if($gt->status == 0 || $gt->status == 1)
                                                                        <h4 class="mb-0"><span class="badge badge-danger-bold">Non Aktif</span></h4>
                                                                        @else
                                                                        <h4 class="mb-0"><span class="badge badge-success-bold">Aktif</span></h4>
                                                                        @endif
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <button type="button" class="btn btn-info shadow-none btn-icon-only" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                                                                        <div class="dropdown-menu">
                                                                            @if($gt->status == 1)
                                                                            <button type="button" data-target=".modal-tambah-device-tangki" data-toggle="modal" class="dropdown-item btn_tambah_device_tangki" data-id="{{$gt->id}}" data-nama_tangki="{{$gt->nama_tangki}} | {{$gt->jenis_tangki}}"><i class="fas fa-clipboard-check"></i>Tambah Device</button>
                                                                            <button type="button" data-target=".modal-hapus-tangki" data-toggle="modal" class="dropdown-item text-danger btn_hapus_tangki" data-id="{{$gt->id}}"><i class="fas fa-trash"></i>Hapus Tangki</button>
                                                                            @elseif($gt->status == 2)
                                                                            <button type="button" data-target=".modal-cabut-device" data-toggle="modal" class="dropdown-item btn_cabut_device" data-id_monitor="{{$get_tandon_aktif_1->id}}"><i class="fas fa-exchange-alt"></i>Dismantle Device</button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="tabs-icons-mts-2" role="tabpanel" aria-labelledby="tabs-icons-mts-2-tab">
                                                    <div class="table-responsive pt-3 pb-3">
                                                        <table class=" table table-flush datatable-basic">
                                                            <thead class="thead-light">
                                                                <tr class="text-center">
                                                                    <th>#</th>
                                                                    <th>Tangki</th>
                                                                    <th>Kapasitas</th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @php
                                                                $index = 1;
                                                                @endphp

                                                                @foreach($get_tangki_aktif as $gta)
                                                                <tr>
                                                                    <td class="text-muted text-center">{{$index++}}</td>
                                                                    <td>
                                                                        <div class="media">
                                                                            <img class="avatar avatar-sm mb-0 bg-transparent" src="{{asset('argon')}}/img/icons/storage-tank.png">
                                                                            <div class="media-body ml-3">
                                                                                <div class="row align-items-center">
                                                                                    <div class="col-lg-12 col-12">
                                                                                        <a href="#!"><h5 class="mb-0">{{$gta->nama_tangki}}</h5></a>
                                                                                        <span class="text-xs text-muted">Total Kapasitas : <b>{{$gta->max_kapasitas}} L</b></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        @if($gta->status == 1)
                                                                        <span class="text-muted text-sm">-</span>
                                                                        @else
                                                                        @php
                                                                        $get_tandon_aktif_2 = App\Monitor::where('id_tangki',$gta->id)->first();
                                                                        @endphp
                                                                        <div class="row">
                                                                            <div class="col-lg-12 col-12 mb-3">
                                                                                <div class="progress-wrapper pt-0">
                                                                                    <div class="progress-info">
                                                                                        <div class="progress-percentage">
                                                                                            <span class="text-xs font-weight-bold text-default">Terpakai : {{round(($get_tandon_aktif_2->cur_cap/$gta->max_kapasitas)*100,2)}}% <sup><i class="fas fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="{{$get_tandon_aktif_2->cur_cap}} L"></i></sup></span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="progress">
                                                                                        <div class="progress-bar bg-success" role="progressbar" aria-valuenow="{{($get_tandon_aktif_2->cur_cap/$gta->max_kapasitas)*100}}" aria-valuemin="0" aria-valuemax="100" style="width: {{round(($get_tandon_aktif_2->cur_cap/$gta->max_kapasitas)*100,2)}}%;"></div>
                                                                                    </div>
                                                                                    <span class="text-xs text-muted">Tersisa : {{$gta->max_kapasitas-$get_tandon_aktif_2->cur_cap}} L</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12 col-12">
                                                                                <div class="row">
                                                                                    <div class="col-auto">
                                                                                        <div class="media align-items-center">
                                                                                            <div class="icon icon-xs icon-shape badge-primary shadow-none">
                                                                                                <i class="fas fa-thermometer-empty"></i>
                                                                                            </div>
                                                                                            <div class="media-body ml-2">
                                                                                                <span class="text-xs font-weight-bold text-default">Suhu Air</span>
                                                                                                <h4 class="mb-0 mt--1 font-weight-bolder">{{$get_tandon_aktif_2->cur_temp}} °</h4>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-auto">
                                                                                        <div class="media align-items-center">
                                                                                            <div class="icon icon-xs icon-shape badge-primary shadow-none">
                                                                                                <i class="fas fa-water"></i>
                                                                                            </div>
                                                                                            <div class="media-body ml-2">
                                                                                                <span class="text-xs font-weight-bold text-default">Tinggi Air</span>
                                                                                                <h4 class="mb-0 mt--1 font-weight-bolder">{{$get_tandon_aktif_2->cur_height}} M</h4>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        @endif 
                                                                    </td>
                                                                    <td class="text-center">
                                                                        @if($gta->status == 0 || $gta->status == 1)
                                                                        <h4 class="mb-0"><span class="badge badge-danger-bold">Non Aktif</span></h4>
                                                                        @else
                                                                        <h4 class="mb-0"><span class="badge badge-success-bold">Aktif</span></h4>
                                                                        @endif
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <button type="button" class="btn btn-info shadow-none btn-icon-only" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                                                                        <div class="dropdown-menu">
                                                                            @if($gta->status == 1)
                                                                            <button type="button" data-target=".modal-tambah-device-tangki" data-toggle="modal" class="dropdown-item btn_tambah_device_tangki" data-id="{{$gta->id}}" data-nama_tangki="{{$gta->nama_tangki}} | {{$gta->jenis_tangki}}"><i class="fas fa-clipboard-check"></i>Tambah Device</button>
                                                                            <button type="button" data-target=".modal-hapus-tangki" data-toggle="modal" class="dropdown-item text-danger btn_hapus_tangki" data-id="{{$gta->id}}"><i class="fas fa-trash"></i>Hapus Tangki</button>
                                                                            @elseif($gta->status == 2)
                                                                            <button type="button" data-target=".modal-cabut-device" data-toggle="modal" class="dropdown-item btn_cabut_device" data-id_monitor="{{$get_tandon_aktif_2->id}}"><i class="fas fa-exchange-alt"></i>Dismantle Device</button>
                                                                            @endif
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('inventori.add.modal-tambah-tangki')
@include('inventori.add.modal-tambah-device-tangki')
@include('inventori.add.modal-hapus-tangki')
@include('inventori.add.modal-cabut-device')
@include('inventori.add.modal-tambah-monitor')

<script type="text/javascript">
    $(document).ready(function() {
        var btn_hapus_tangki = $('.btn_hapus_tangki');
        btn_hapus_tangki.click(function(){
            var id_tangki = $(this).data('id')
            $('.id_tangki').val(id_tangki)
        });
        var btn_tambah_device_tangki = $('.btn_tambah_device_tangki');
        btn_tambah_device_tangki.click(function(){
            var id_tangki = $(this).data('id')
            $('.id_tangki').val(id_tangki)

            var nama_tangki = $(this).data('nama_tangki')
            $('.nama_tangki').val(nama_tangki)
        });
        var btn_cabut_device = $('.btn_cabut_device');
        btn_cabut_device.click(function(){
            var id_monitor = $(this).data('id_monitor')
            $('.id_monitor').val(id_monitor)
        });
    });
</script>
@endsection