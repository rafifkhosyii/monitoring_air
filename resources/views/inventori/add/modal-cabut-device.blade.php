<div class="modal fade modal-cabut-device" tabindex="-1" role="dialog" aria-labelledby="label-modal-cabut-device" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="label-modal-cabut-device">Dismantle Device</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cabut-device" method="POST" enctype="multipart/form-data" autocomplete="autocomplete">
        @csrf
        <input type="hidden" name="id_monitor" class="id_monitor">
        <div class="modal-body">
          <div class="row mb-4">
            <div class="col-lg-12 col-12 text-center">
              <div class="icon icon-lg icon-shape badge-info shadow-none">
                <i class="fas fa-exchange-alt"></i>
              </div>
              <h3 class="mb-0 mt-4">Anda akan melepaskan device pada data tangki berikut ?</h3>
              <span class="text-sm text-muted">Data yang telah diubah tidak dapat bisa dikembalikan lagi</span>
            </div>
          </div>
          <div class="row justify-content-center mb-2">
            <div class="col-lg-4 col-12">
              <button type="button" class="shadow-none btn-block btn btn-link text-muted" data-dismiss="modal">Close</button>
            </div>
            <div class="col-lg-4 col-12">
              <button type="submit" class="shadow-none btn-block btn btn-success">Submit</button>     
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>