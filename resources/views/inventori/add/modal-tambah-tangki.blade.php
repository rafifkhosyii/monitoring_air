<div class="modal fade modal-tambah-tangki" tabindex="-1" role="dialog" aria-labelledby="label-modal-tambah-tangki" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="label-modal-tambah-tangki">Tambah Tangki Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/tambah-tangki" method="POST" enctype="multipart/form-data" autocomplete="autocomplete">
        @csrf
        <div class="modal-body">
          <div class="row justify-content-center mb-2">
            <div class="col-lg-11 col-12">
              <div class="row">
                <div class="col-lg-12 col-12">
                  <div class="row">
                    <div class="col-lg-12 col-12">
                      <div class="form-group">
                        <label class="form-control-label" for="nama_tangki">Nama Tangki</label><sup class="text-danger"> *</sup>
                        <input class="form-control {{ $errors->has('nama_tangki') ? ' is-invalid' : '' }} " name="nama_tangki"  type="text" required>
                        @if ($errors->has('nama_tangki'))
                        <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('nama_tangki') }}</strong></span>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-6 col-12">
                      <div class="form-group">
                        <label class="form-control-label" for="jenis_tangki">Jenis</label><sup class="text-danger"> *</sup>
                        <select class="form-control {{ $errors->has('jenis_tangki') ? 'is-invalid' : ''}} jenis_tangki" data-toggle="select" name="jenis_tangki">
                          <option value="Tandon Air Fiber Glass">Tandon Air Fiber Glass</option>
                          <option value="Tandon Air Polyethylene">Tandon Air Polyethylene</option>
                          <option value="Tandon Air Stainless Steel">Tandon Air Stainless Steel</option>
                        </select>
                        @if ($errors->has('jenis_tangki'))
                        <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('jenis_tangki') }}</strong></span>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-6 col-12">
                      <div class="form-group">
                        <label class="form-control-label" for="max_kapasitas">Kapasitas Maksimal</label><sup class="text-danger"> *</sup>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="max_kapasitas">L</span>
                          </div>
                          <input type="number" name="max_kapasitas"  class="form-control"  aria-label="max_kapasitas" aria-describedby="max_kapasitas">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-center mb-2">
            <div class="col-lg-4 col-12">
              <button type="button" class="btn shadow-none btn-block btn-link text-muted" data-dismiss="modal">Close</button>
            </div>
            <div class="col-lg-4 col-12">
              <button type="submit" class="btn shadow-none btn-block btn-success">Submit</button>     
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

