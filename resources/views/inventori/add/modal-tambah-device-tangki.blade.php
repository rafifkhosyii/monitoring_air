@php
$data_device = App\Device::where('status',1)->get();
@endphp
<div class="modal fade modal-tambah-device-tangki" tabindex="-1" role="dialog" aria-labelledby="label-modal-tambah-device-tangki" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="label-modal-tambah-device-tangki">Monitor Tangki Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/tambah-monitor" method="POST" enctype="multipart/form-data" autocomplete="autocomplete">
        @csrf
        <div class="modal-body">
          <input type="hidden" name="id_tangki" class="id_tangki">
          <div class="row justify-content-center mb-2">
            <div class="col-lg-11 col-12">
              <div class="row">
                <div class="col-lg-12 col-12">
                  <div class="row">
                    <div class="col-lg-12 col-12">
                      <div class="form-group">
                        <label class="form-control-label" for="nama_tangki">Nama Tangki</label><sup class="text-danger"> *</sup>
                        <input class="form-control {{ $errors->has('nama_tangki') ? 'is-invalid' : ''}} nama_tangki" type="text" readonly name="nama_tangki">
                        @if ($errors->has('nama_tangki'))
                        <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('nama_tangki') }}</strong></span>
                        @endif
                      </div>
                    </div>
                    <div class="col-lg-12 col-12">
                      <div class="form-group">
                        <label class="form-control-label" for="id_device">Device</label><sup class="text-danger"> *</sup>
                        <select class="form-control {{ $errors->has('id_device') ? 'is-invalid' : ''}} id_device" data-toggle="select" name="id_device">
                          @foreach($data_device as $dv)
                          <option value="{{$dv->id}}">{{$dv->nama_device}} | {{$dv->kode_device}} (x {{$dv->qty}}) </option>
                          @endforeach
                        </select>
                        @if ($errors->has('id_device'))
                        <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('id_device') }}</strong></span>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-center mb-2">
            <div class="col-lg-4 col-12">
              <button type="button" class="btn shadow-none btn-block btn-link text-muted" data-dismiss="modal">Close</button>
            </div>
            <div class="col-lg-4 col-12">
              <button type="submit" class="btn shadow-none btn-block btn-success">Submit</button>     
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

