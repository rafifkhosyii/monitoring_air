<div class="modal fade modal-tambah-stock-device" tabindex="-1" role="dialog" aria-labelledby="label-modal-tambah-stock-device" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="label-modal-tambah-stock-device">Tambah Stock Device</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/tambah-stock-device" method="POST" enctype="multipart/form-data" autocomplete="autocomplete">
        @csrf
        <div class="modal-body">
          <input type="hidden" name="id_device" class="id_device">
          <div class="row justify-content-center mb-2">
            <div class="col-lg-11 col-12">
              <div class="row">
                <div class="col-lg-12 col-12">
                  <div class="row">
                    <div class="col-lg-4 col-12">
                      <div class="form-group">
                        <label class="form-control-label" for="qty">Jumlah</label><sup class="text-danger"> *</sup>
                        <input class="form-control {{ $errors->has('qty') ? ' is-invalid' : '' }} " name="qty"  type="number" required>
                        @if ($errors->has('qty'))
                        <span class="invalid-feedback" role="alert"><strong>{{ $errors->first('qty') }}</strong></span>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-center mb-2">
            <div class="col-lg-4 col-12">
              <button type="button" class="btn shadow-none btn-block btn-link text-muted" data-dismiss="modal">Close</button>
            </div>
            <div class="col-lg-4 col-12">
              <button type="submit" class="btn shadow-none btn-block btn-success">Submit</button>     
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

