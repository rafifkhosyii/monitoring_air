@extends('layouts.app')

@section('navbar')
@include('layouts.navbar')
@endsection

@section('sidebar')

@endsection

@section('content')

@php
$today = Carbon\Carbon::today();
@endphp

<div class="container-fluid pt-5 pb-7 bg-white">
    <div class="row justify-content-center">
        <div class="col-lg-11 col-12">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-12 mb-4">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-12 mb-4">
                                    <div class="row align-items-center">
                                        <div class="col-lg-12 col-12 mb-lg-0 mb-2">
                                            <span class="display-3 font-weight-bolder text-gray-dark">Device</span>
                                            <div class="mt--1" style="height: 10px; background-color: blue; width: 50px;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="card shadow-none badge-info" style=" background-size:45%; background-repeat:no-repeat; background-position: -30px 50%;background-image: url({{asset('argon')}}/img/icons/iot.png">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-7 col-7 offset-5 offset-md-5 offset-lg-6">
                                                    <div class="row justify-content-between">
                                                        <div class="col-lg-12 col-12 pb-0 pb-lg-4">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-12">
                                                                    <h4 class="mb--1">Total Device</h4>
                                                                    <span class="display-2 font-weight-bolder mb-0">{{$get_device->count()}}</span>
                                                                </div>
                                                                <div class="col-lg-6 col-12 text-right">
                                                                    <button type="button" class="btn btn-info shadow-none btn-icon-only" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-plus"></i></button>
                                                                    <div class="dropdown-menu">
                                                                        <button class="dropdown-item" data-target=".modal-tambah-device" data-toggle="modal"><i class="fas fa-plus"></i>Tambah Device Baru</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-12 mb-4">
                                    <div class="row align-items-center">
                                        <div class="col-lg-12 col-12">
                                            <div class="table-responsive pt-3 pb-3">
                                                <table class=" table table-flush datatable-basic">
                                                    <thead class="thead-light">
                                                        <tr class="text-center">
                                                            <th>#</th>
                                                            <th>Device</th>
                                                            <th>Qty</th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                        $index = 1;
                                                        @endphp

                                                        @foreach($get_device as $gd)
                                                        <tr>
                                                            <td class="text-muted text-center">{{$index++}}</td>
                                                            <td>
                                                                <div class="media">
                                                                    <img class="avatar avatar-sm mb-0 bg-transparent" src="{{asset('argon')}}/img/icons/iot.png">
                                                                    <div class="media-body ml-3">
                                                                        <div class="row align-items-center">
                                                                            <div class="col-lg-12 col-12">
                                                                                <a href="#!"><h5 class="mb-0">{{$gd->nama_device}}</h5></a>
                                                                                <span class="text-xs text-muted">{{$gd->kode_device}}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <h4 class="mb-0">x {{$gd->qty}}</h4>
                                                            </td>
                                                            <td class="text-center">
                                                                @if($gd->status == 1)
                                                                <h4 class="mb-0"><span class="badge badge-success-bold">Tersedia</span></h4>
                                                                @else
                                                                <h4 class="mb-0"><span class="badge badge-danger-bold">Kosong</span></h4>
                                                                @endif
                                                            </td>
                                                            <td class="text-right">
                                                                <button type="button" class="btn btn-info shadow-none btn-icon-only" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu">
                                                                    <button type="button" data-target=".modal-tambah-stock-device" data-toggle="modal" class="dropdown-item btn_tambah_stock_device" data-id="{{$gd->id}}"><i class="fas fa-plus"></i>Tambah Stok</button>
                                                                    <button type="button" data-target=".modal-hapus-device" data-toggle="modal" class="dropdown-item btn_hapus_device text-danger" data-id="{{$gd->id}}"><i class="fas fa-trash"></i>Hapus Device</button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('inventori.add.modal-tambah-device')
@include('inventori.add.modal-hapus-device')
@include('inventori.add.modal-tambah-stock-device')

<script type="text/javascript">
    $(document).ready(function() {
        var btn_tambah_stock_device = $('.btn_tambah_stock_device');
        btn_tambah_stock_device.click(function(){
            var id_device = $(this).data('id')
            $('.id_device').val(id_device)
        });
        var btn_hapus_device = $('.btn_hapus_device');
        btn_hapus_device.click(function(){
            var id_device = $(this).data('id')
            $('.id_device').val(id_device)
        });
    });
</script>

@endsection