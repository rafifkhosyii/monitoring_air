@extends('layouts.app')

@section('content')
<div class="container-fluid d-none d-lg-block">
  <div class="split-40 left-0" style="background-repeat: no-repeat ; background-size: cover ;background-image: url({{asset('argon')}}/img/theme/wave.svg">
  </div>
  <div class="split-60 right-0">
    <div class="row justify-content-center centered">
      <div class="col-lg-8 mb-2 text-center">
        <img src="{{asset('argon')}}/img/theme/128.png" >
        <h1 class="mb-0">Selamat Datang</h1>
      </div>
      <div class="col-lg-7 col-12">
        <form method="post" action="{{ route('login') }}" autocomplete="off" enctype= multipart/form-data>
          @csrf
          <div class="row">
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-email">Email</label>
                    <input  type="email" class="form-control @error('email') is-invalid @enderror" name="email" required>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label class="form-control-label" for="input-password">Password</label>
                    <input  type="password" class="form-control @error('email') is-invalid @enderror" name="password" required>
                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-6 col-12">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-control-label"  for="remember">
                          Ingat Saya ?
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="row justify-content-center">
                    <div class="col-lg-12 col-12">
                      <button type="submit" class=" btn btn-info btn-block mt-3">Login</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="header d-lg-none">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 540 320"><path d="M0 267L22.5 257.8C45 248.7 90 230.3 135 224.2C180 218 225 224 270 207C315 190 360 150 405 156.5C450 163 495 216 517.5 242.5L540 269L540 0L517.5 0C495 0 450 0 405 0C360 0 315 0 270 0C225 0 180 0 135 0C90 0 45 0 22.5 0L0 0Z" fill="#08b0f9" style="transition: all 0.4s cubic-bezier(0.16, 1, 0.3, 1) 0s;"></path><path d="M0 76L22.5 77C45 78 90 80 135 96.7C180 113.3 225 144.7 270 151.8C315 159 360 142 405 135.8C450 129.7 495 134.3 517.5 136.7L540 139L540 0L517.5 0C495 0 450 0 405 0C360 0 315 0 270 0C225 0 180 0 135 0C90 0 45 0 22.5 0L0 0Z" fill="#08b0f9" style="transition: all 0.4s cubic-bezier(0.16, 1, 0.3, 1) 0s;"></path><path d="M0 128L22.5 111.8C45 95.7 90 63.3 135 64.5C180 65.7 225 100.3 270 104.7C315 109 360 83 405 74.5C450 66 495 75 517.5 79.5L540 84L540 0L517.5 0C495 0 450 0 405 0C360 0 315 0 270 0C225 0 180 0 135 0C90 0 45 0 22.5 0L0 0Z" fill="#004cdf" style="transition: all 0.4s cubic-bezier(0.16, 1, 0.3, 1) 0s;"></path></svg>
</div>

<div class="container-fluid d-lg-none pb-6">
  <div class="row justify-content-center" >
    <div class="col-10">
      <div class="row">
      <div class="col-lg-12 mb-2 text-center">
        <img src="{{asset('argon')}}/img/theme/128.png" >
        <h1 class="mb-0">Selamat Datang</h1>
      </div>
        <div class="col-lg-7 col-12">
          <form method="post" action="{{ route('login') }}" autocomplete="off" enctype= multipart/form-data>
            @csrf
            <div class="row">
              <div class="col-lg-12">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Email</label>
                      <input  type="email" class="form-control @error('email') is-invalid @enderror" name="email" required>
                      @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-password">Password</label>
                      <input  type="password" class="form-control @error('email') is-invalid @enderror" name="password" required>
                      @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-6 col-12">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-control-label"  for="remember">
                          Ingat Saya ?
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                  <div class="col-lg-12">
                    <div class="row justify-content-center">
                      <div class="col-lg-12 col-12">
                      <button type="submit" class=" btn btn-info btn-block mt-3">Login</button>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
