@extends('layouts.app')

@section('navbar')
@include('layouts.navbar')
@endsection

@section('sidebar')

@endsection

@section('content')

@php
$today = Carbon\Carbon::today();
@endphp

<div class="container-fluid pt-5 pb-7 bg-white">
    <div class="row justify-content-center">
        <div class="col-lg-11 col-12">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-12 mb-4">
                    <div class="card shadow-none">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 col-12 mb-4">
                                    <form action="#!" method="GET" enctype="multipart/form-data">
                                        <div class="row align-items-center">
                                            <div class="col-lg-12 col-12 mb-lg-0 mb-2">
                                                <span class="display-3 font-weight-bolder text-gray-dark">Overview</span>
                                                <div class="mt--1" style="height: 10px; background-color: blue; width: 50px;"></div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- <hr class="mt-4 mb-0" style="border-style:dashed;"> -->
                                </div>
                                <div class="col-lg-4 col-12 mb-4 mb-lg-0">
                                    <div class="row">
                                        <div class="col-lg-12 col-12 mb-3">
                                            <div class="row align-items-center">
                                                <div class="col-lg-12 col-12">
                                                    <div class="media align-items-center">
                                                        <img src="{{asset('argon')}}/img/icons/siang.svg" class="avatar avatar-sm bg-transparent">
                                                        <div class="media-body ml-3">
                                                            <span class="mb-0 text-sm font-weight-500">Hari ini, {{$today->format('d-M-y')}}</span>
                                                            <h3 class="mb-0 mt--1 font-weight-bolder text-info">{{rand(20,30)}} °</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-12">
                                                    <div class="card shadow-none bg-info">
                                                        <div class="card-body">
                                                            <div class="row align-items-center">
                                                                <div class="col-lg-12 col-12">
                                                                    <div class="icon icon-shape badge-info shadow-none">
                                                                        <i class="fas fa-prescription-bottle"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row mt-5 align-items-center">
                                                                <div class="col-lg-9 col-9">
                                                                    <h1 class="display-3 mb--2 font-weight-bolder text-white">{{$get_monitor->count()}}</h1>
                                                                    <span class="text-sm font-weight-500 text-white">Tangki Aktif</span>
                                                                </div>
                                                                <div class="col-lg-3 col-3 text-right">
                                                                    <a href="/tangki" class="shadow-none btn btn-secondary btn-sm btn-icon-only rounded-circle"><i class="fas fa-chevron-right" data-toggle="tooltip" data-placement="top" title="Selengkapnya"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-12 mt-2">
                                                    <div class="row align-items-center">
                                                        <div class="col-lg-6 col-12 mt-2">
                                                            <div class="media align-items-center">
                                                                <a href="/device">
                                                                    <div class="icon icon-shape badge-primary shadow-none">
                                                                        <i class="fas fa-mobile"></i>
                                                                    </div>
                                                                </a>
                                                                <div class="media-body ml-3">
                                                                    <span class="mb-0 text-sm font-weight-500">Jenis Device</span>
                                                                    <h3 class="mb-0 mt--1 font-weight-bolder">{{$get_device->count()}}</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       <!--  <div class="col-lg-6 col-12 mt-2">
                                                            <div class="media align-items-center">
                                                                <span class="display-3">68 %</span>
                                                                <div class="media-body ml-3">
                                                                    <span class="mb-0 text-sm font-weight-500">Kapasitas</span>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-12 offset-lg-1">
                                    <h3 class="font-weight-bolder">Chart</h3>
                                    <div class="chart mt-3">
                                        <canvas class="chart-canvas chart-bars"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-12">
                    <hr class="mt-0 mb-0" style="border-style: dashed;">
                    <div class="row">
                        <div class="col-lg-8 col-12 mt-4">
                            <div class="row align-items-center">
                                <div class="col-lg-9 col-9">
                                    <h3 class="font-weight-bolder pl-0 pl-lg-3">Montior Tangki</h3>
                                </div>
                                <div class="col-lg-3 col-3 text-right">
                                </div>
                                <div class="col-lg-12 col-12">
                                    <div class="table-responsive pt-3 pb-3">
                                        <table class=" table table-flush datatable-basic">
                                            <thead class="thead-light">
                                                <tr class="text-center">
                                                    <th>#</th>
                                                    <th>Tangki</th>
                                                    <th>Kapasitas</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $index = 1;
                                                @endphp

                                                @foreach($get_monitor as $gt)
                                                <tr>
                                                    <td class="text-muted text-center">{{$index++}}</td>
                                                    <td>
                                                        <div class="media">
                                                            <img class="avatar avatar-sm mb-0 bg-transparent" src="{{asset('argon')}}/img/icons/storage-tank.png">
                                                            <div class="media-body ml-3">
                                                                <div class="row align-items-center">
                                                                    <div class="col-lg-12 col-12">
                                                                        <a href="#!"><h5 class="mb-0">{{$gt->nama_tangki}}</h5></a>
                                                                        <span class="text-xs text-muted">Total Kapasitas : <b>{{$gt->max_kapasitas}} L</b></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        @if($gt->status == 1)
                                                        <span class="text-muted text-sm">-</span>
                                                        @else
                                                        @php
                                                        $get_tandon_aktif_1 = App\Monitor::where('id_tangki',$gt->id)->first();
                                                        @endphp
                                                        <div class="row">
                                                            <div class="col-lg-12 col-12 mb-3">
                                                                <div class="progress-wrapper pt-0">
                                                                    <div class="progress-info">
                                                                        <div class="progress-percentage">
                                                                            <span class="text-xs font-weight-bold text-default">Terpakai : {{round(($get_tandon_aktif_1->cur_cap/$gt->max_kapasitas)*100,2)}}% <sup><i class="fas fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="{{$get_tandon_aktif_1->cur_cap}} L"></i></sup></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="progress">
                                                                        <div class="progress-bar bg-success" role="progressbar" aria-valuenow="{{($get_tandon_aktif_1->cur_cap/$gt->max_kapasitas)*100}}" aria-valuemin="0" aria-valuemax="100" style="width: {{round(($get_tandon_aktif_1->cur_cap/$gt->max_kapasitas)*100,2)}}%;"></div>
                                                                    </div>
                                                                    <span class="text-xs text-muted">Tersisa : {{$gt->max_kapasitas-$get_tandon_aktif_1->cur_cap}} L</span>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-12">
                                                                <div class="row">
                                                                    <div class="col-auto">
                                                                        <div class="media align-items-center">
                                                                            <div class="icon icon-xs icon-shape badge-primary shadow-none">
                                                                                <i class="fas fa-thermometer-empty"></i>
                                                                            </div>
                                                                            <div class="media-body ml-2">
                                                                                <span class="text-xs font-weight-bold text-default">Suhu Air</span>
                                                                                <h4 class="mb-0 mt--1 font-weight-bolder">{{$get_tandon_aktif_1->cur_temp}} °</h4>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-auto">
                                                                        <div class="media align-items-center">
                                                                            <div class="icon icon-xs icon-shape badge-primary shadow-none">
                                                                                <i class="fas fa-water"></i>
                                                                            </div>
                                                                            <div class="media-body ml-2">
                                                                                <span class="text-xs font-weight-bold text-default">Tinggi Air</span>
                                                                                <h4 class="mb-0 mt--1 font-weight-bolder">{{$get_tandon_aktif_1->cur_height}} M</h4>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif 
                                                    </td>
                                                    <td class="text-center">
                                                        @if($gt->status == 0 || $gt->status == 1)
                                                        <h4 class="mb-0"><span class="badge badge-danger-bold">Non Aktif</span></h4>
                                                        @else
                                                        <h4 class="mb-0"><span class="badge badge-success-bold">Aktif</span></h4>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-12 mt-4">
                            <div class="row align-items-center">
                                <div class="col-lg-9 col-9">
                                    <h3 class="font-weight-bolder pl-0 pl-lg-3">Aktifitas Terbaru</h3>
                                </div>
                                <div class="col-lg-2 col-3 text-right">
                                    <a href=".modal-list-log-monitor" data-toggle="modal" class="text-muted btn_list_log_monitor"><i class="fas fa-eye" data-toggle="tooltip" data-placement="top" title="Selengkapnya"></i></a>
                                </div>
                                <div class="col-lg-12 col-12 mt-3 pl-0 pl-lg-5">
                                    <div class="timeline timeline-one-side mb-2" data-timeline-content="axis" data-timeline-axis-style="dashed">
                                        @foreach($get_log as $gl)
                                        <div class="timeline-block">
                                            <span class="timeline-step">
                                                <span class="badge badge-info text-xs font-weight-500">{{$gl->created_at->format('H:i')}}</span>
                                            </span>
                                            <div class="timeline-content pt-1">
                                                <div class="row align-items-center">
                                                    <div class="col text-wrap">
                                                        <h5 class="mb-0 font-weight-500">{{$gl->deskripsi}}</h5>
                                                        <h6 class="mb-0 font-weight-500 text-muted">{{$gl->created_at}}</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('monitor.add.modal-list-log-monitor')

<script type="text/javascript">
    $(document).ready(function() {
        var btn_list_log_monitor = $('.btn_list_log_monitor');
        btn_list_log_monitor.click(function(){
            fetch('/api/list-log-monitor', {
                method: 'get'
            })
            .then(response => response.json())
            .then(data => {
                let tableItemRequest = ''
                for(let i = 0; i < data.length; i++){
                    let no = i+1;
                    tableItemRequest += '<tr><td class="text-muted text-center">'+no+'</td><td><h5 class="mb-0 font-weight-500">'+data[i]['deskripsi']+'</h5></td><td class="text-center"><span class="text-sm text-muted">'+data[i]['created_at']+'</span></td></tr>'
                }
                $('.tableItemRequest').html(tableItemRequest)
            }).catch(err => {
                console.log(err);
            })  
        });
    });
</script>

@endsection