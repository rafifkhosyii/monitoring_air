<div class="modal fade modal-list-log-monitor" id="modal-list-log-monitor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="label-modal-list-log-monitor">Log Aktifitas Monitor</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
          <div class="row justify-content-center mb-2">
            <div class="col-lg-11 col-12">
              <div class="row">
                <div class="col-lg-12 col-12">
                  <div class="table-responsive pt-3 pb-3">
                    <table class="table table-flush">
                      <thead class="thead-light">
                        <tr class="text-center">
                          <th>#</th>
                          <th>Deskripsi</th>
                          <th>Waktu</th>
                        </tr>
                      </thead>
                      <tbody class="tableItemRequest">
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-center mb-2">
            <div class="col-lg-4 col-12">
              <button type="button" class="btn shadow-none btn-block btn-link text-muted" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>